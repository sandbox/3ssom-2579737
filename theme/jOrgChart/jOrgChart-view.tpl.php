    <?php

    drupal_add_css(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/css/bootstrap.min.css","file");
    drupal_add_css(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/css/jquery.jOrgChart.css","file");
    drupal_add_css(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/css/custom.css","file");

    drupal_add_css(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/css/prettify.css","file");

    drupal_add_js(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/js/prettify.js","file");
    drupal_add_js(drupal_get_path('module', 'taxonomy_tree') . "/theme/jOrgChart/js/jquery.jOrgChart.js","file");

    ?>

    <script>
    jQuery(document).ready(function() {
        jQuery("#org").jOrgChart({
            chartElement : '#chart',
            dragAndDrop  : false
        });
    });
    </script>


  <body onload="prettyPrint();">
    

<div class="structure-tree well">


   <ul id="org" style="display:none">
<?php foreach ($data as $parent):?>
        <li><?php $pic = _get_term_pic($parent->tid); ?>
            <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $parent->name;?></span>
            <ul>
              <?php foreach ($parent->child as $child):?>
                <li><?php $pic = _get_term_pic($child->tid); ?>
                  <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $child->name;?></span>
                    <ul>
                      <?php foreach ($child->level2 as $level2):?>
                        <li><?php $pic = _get_term_pic($level2->tid); ?>
                          <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $level2->name; ?></span>
                                <ul>
                                  <?php foreach ($level2->level3 as $level3):?>
                                    <li><?php $pic = _get_term_pic($level3->tid); ?>
                                      <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $level3->name; ?></span>
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    </li>
             <?php endforeach; ?></ul>
        </li>
    <?php endforeach; ?>
</ul>
       
    
    <div id="chart" class="orgChart"></div>
    
    <script>
        jQuery(document).ready(function() {
            
            /* Custom jQuery for the example */
            jQuery("#show-list").click(function(e){
                e.preventDefault();
                
                jQuery('#list-html').toggle('fast', function(){
                    if(jQuery(this).is(':visible')){
                        jQuery('#show-list').text('Hide underlying list.');
                        jQuery(".topbar").fadeTo('fast',0.9);
                    }else{
                        jQuery('#show-list').text('Show underlying list.');
                        jQuery(".topbar").fadeTo('fast',1);                  
                    }
                });
            });
            
            jQuery('#list-html').text(jQuery('#org').html());
            
            jQuery("#org").bind("DOMSubtreeModified", function() {
                jQuery('#list-html').text('');
                
                jQuery('#list-html').text(jQuery('#org').html());
                
                prettyPrint();                
            });
        });
    </script>
    </div> 
  </body>