<div class="structure-tree well">

   <ul>
<?php foreach ($data as $parent):?>
        <li class="tree-color1"><?php $pic = _get_term_pic($parent->tid); ?>
            <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $parent->name;?></span>
            <ul>
              <?php foreach ($parent->child as $child):?>
                <li class="tree-color2"><?php $pic = _get_term_pic($child->tid); ?>
                  <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $child->name;?></span>
                    <ul>
                      <?php foreach ($child->level2 as $level2):?>
                        <li class="tree-color3"><?php $pic = _get_term_pic($level2->tid); ?>
                          <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $level2->name; ?></span>
                                <ul>
                                  <?php foreach ($level2->level3 as $level3):?>
                                    <li class="tree-color4"><?php $pic = _get_term_pic($level3->tid); ?>
                                      <span><?php if($pic){ ?><img src="<?php print $pic; ?>"><?php } ?><?php print $level3->name; ?></span>
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    </li>
             <?php endforeach; ?></ul>
        </li>
    <?php endforeach; ?>
</ul>
</div>

