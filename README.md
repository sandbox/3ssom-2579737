-- SUMMARY --

This module provides Structure Tree Based On taxonomy trems using JS library to display your terms nicely and tree collapse style.
the module for now support only four levels in your taxonomy terms .. so the fifth level won't show if you add it.


-- REQUIREMENTS --

taxonomy
i18n
image
i18n_taxonomy
i18n_string

-- INSTALLATION --

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.


-- CONFIGURATION --

 - You can reach Taxonomy Tree settings by going to this path: Administration » Configuration » Content authoring » Taxonomy Tree Settings.
 - from your settings can configure which taxonomy terms you want to display ,, also you can show a default image if the term has no image uploaded along with the option to show the default image or disable showing it all.
 - You Can give permissions to specific role by giving them 'Taxonomy Tree Settings'.
 - You have a menu showing after enabling the module in this path: structure/tree.


-- CUSTOMIZATION --

 - The module support localizing trems using i18n modules ,, so you can translate your terms to other languages by chosing ' Localize. Terms are common for all languages, but their name and description may be localized.' option in your taxonomy edit options.
 - If you need an image configured for every term you'll need to add a Image field in your selected Taxonomy .. and you HAVE TO NAME IT (tax_image) so the field will be renamed by default to this(field_tax_image) .. that's it .. enjoy.


-- CONTACT --

Current maintainer:
* Essam AlQaie (3ssom) - https://www.drupal.org/u/3ssom

